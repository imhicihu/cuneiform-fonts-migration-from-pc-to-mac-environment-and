* Software
    - Backup
        - [Duplicati](https://www.duplicati.com/): open-source backup software
    - Font management:
        - [FontBase](https://fontba.se/)
    - Font testing
        - [Font inter/lab](https://rsms.me/inter/lab/?sample=Default&repertoireOrder=&size=13&weight=400&letterSpacing=0&antialias=greyscale&text-decoration=none&text-transform=none&variantLigatures=normal&variantCaps=normal&variantNumeric=normal&rasterizePhrase=Account%20expiration): font testing, kerning _et alia_
    - Font conversion:
        - [Fontplop](http://www.fontplop.com/): an OSX/macOS application which takes ttf and otf files and outputs a webfont bundle: `woff2`, `woff`, `ttf`, `svg`, and `eot`
    
* Online tools
    - [Plantuml](http://www.plantuml.com/plantuml/uml/): Diagram / deployment diagram / critical path visualization