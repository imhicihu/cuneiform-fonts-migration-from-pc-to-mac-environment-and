![stability-work_in_progress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

# Rationale #

* Process involved in the migration of true type fonts (`.ttf`) from the Windows environment to MacOSX
* Essentially from the Winglyph (Windows true type font based) to `-otf` file format 
* ![sumerian.jpg](https://bitbucket.org/repo/75nodd/images/2489898852-exported%20fonts.jpg)

### What is this repository for? ###

* Quick summary
   - Migrate true type fonts to the MacOSX environment in the safest way as possible  
* Version
   - 1.0

### Related repositories ###

* Some repositories linked with this project:
     - [Winglyph (Virtualization)](https://bitbucket.org/imhicihu/winglyph-virtualization/src/)
     - [IMHICIHU webfont](https://bitbucket.org/imhicihu/imhicihu-webfont/src/)

### How do I get set up? ###

* Summary of set up
     - Interrupted (by now)
* Configuration
     - Interrupted (by now)
* Dependencies
     - Interrupted (by now)
* Deployment instructions
     - Interrupted (by now)

### Who do I talk to? ###

* Repo owner or admin
     - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
     - (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/cuneiform-fonts-migration-from-pc-to-mac-environment-and/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners. 